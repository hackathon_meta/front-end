import React from 'react';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';

import Home from './pages/Home'
import CadLeaguers from './pages/CadLeaguers'
import Leaguers from './pages/Leaguers'
import ListUsers from './pages/ListUsers'
import Form from './pages/Form'
import NotFound from './pages/NotFound'

import { isAuthenticated } from "./services/auth"

const PrivateRoute = ({Component}) => {
 
    return isAuthenticated() ? ( <Component />) : <Navigate to="/"/>
  
}
const RoutesApp  = () => {
  
        return (
            <BrowserRouter>
            <Routes>
              <Route path="/" element={<Home />} />  
              <Route path="/cadleaguers" element ={<PrivateRoute Component={CadLeaguers} />} />  
              <Route path="/leaguers" element ={<PrivateRoute Component={Leaguers} />} /> 
              <Route path="/listusers" element ={<PrivateRoute Component={ListUsers} />} />
              <Route path="*" element={<NotFound />} />     
              <Route path="/form" element={<Form />} />      
            </Routes>
          </BrowserRouter> 
        );
    }

export default RoutesApp;