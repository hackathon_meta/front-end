import styled from 'styled-components';

export const Container = styled.div`
  display: grid;
  grid:
    "nav nav nav" min-content
    "header main main" 1fr / min-content 1fr;
    min-height: 100vh;
`
export const GridNav = styled.div`
  grid-area: nav;
  z-index: 2000;
`;

export const Main = styled.main`
  grid-area: main;
  margin-top : 52px;
  width: 90%;
`;