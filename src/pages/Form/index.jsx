import React from "react";
import Header from "../../components/Header";
import MainForm from "../../components/MainForm";
import { Main, Container, GridNav } from "./styled";

const Form = () => {
  return (
    <Container>
      <GridNav>
        <Header />
      </GridNav>

      <Main>
        <MainForm/>
      </Main>
    </Container>
  );
};

export default Form;