import React from 'react';
import Header from '../../components/Header'
import MainHome from '../../components/MainHome';



import {Container} from './styled'


const Home = () => {

        return (
            <Container>
                <Header/>
                <MainHome/>        
            </Container>
        );
    }

export default Home;
