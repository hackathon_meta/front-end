import React, { useState, useEffect } from 'react'
import Header from '../../components/Header';
import Sidebar from '../../components/Sidebar'
import { Main, Container, GridNav, GridSidebar } from './styled'
import { Table } from 'antd';

import EditUser from '../../components/ModalEditUser'

import api from '../../services/api'

const ListUsers = () => {

  const [users, setUsers] = useState([]);

  const token = localStorage.getItem('token');

  useEffect(() => {

   listUsers();

  }, [token,users]);

  const listUsers = async () => {
    await api.get('users', {
      headers: {
        "Authorization": `Bearer ${token}`
      }
    }).then(response => {
      setUsers(response.data.Users);
    })
  }
  const columns = [
    {
      title: 'Nome',
      dataIndex: 'name',
      filters: users.map(res => {
        return ({
          text: `${res.name}`,
          value: `${res.name}`,
        })
      }),
      filterMode: 'tree',
      filterSearch: true,
      onFilter: (value, record) => record.name.startsWith(value),
      width: '90%',

    },
    {
      filter: () => users.map(res =>
      ({
        text: `${res._id}`,
        value: `${res._id}`,

      }),
      ),
    },

    {

      title: 'Action',
      key: 'operation',
      fixed: 'right',
      width: 100,
      render: (_, record) => <EditUser id={record.id} />,
    },
  ];
  const data = users.map((res) => {
    return ({
      name: `${res.name}`,
      id: `${res._id}`
    })

  });
  const onChange = (pagination, filters, sorter, extra) => {
    console.log('params', pagination, filters, sorter, extra);
  };

  return (

    <Container>
      <GridNav>
        <Header />
      </GridNav>
      <GridSidebar>
        <Sidebar />
      </GridSidebar>
      <Main>
        <Table
          headerStyle={{ backgroundColor: '#0255A7' }}
          columns={columns}
          dataSource={data}
          onChange={onChange}
        />
      </Main>
    </Container>
  )
}

export default ListUsers;

