import styled from 'styled-components'

export const Container = styled.div`
  display: grid;
  grid:
    "nav nav nav" min-content
    "header main main" 1fr / min-content 1fr;
    min-height: 100vh;
`
export const GridNav = styled.div`
  grid-area: nav;
  z-index: 2000;
`;

export const GridSidebar = styled.header`
  grid-area: header;
`;

export const Main = styled.main`
  grid-area: main;
  margin : 100px auto;
  width: 90%;  
`;

export const Button = styled.button`
 width: 160px;
 height: 39px;
 left: 467px;
 top: 316px;
 background: #122870;
 border-radius: 6px;
 border: none;
 cursor: pointer;
 color: #ffffff;

 :hover{
   color:#ffffff;
   font-weight: bold;
   cursor: pointer;
  }
`;