import React from "react";
import Header from "../../components/Header";
import MainCadLeaguer from "../../components/MainCadLeaguer";
import Sidebar from "../../components/Sidebar";
import { Main, Container, GridNav, GridSidebar } from "./styled";

const CadLeaguers = () => {
  return (
    <Container>
      <GridNav>
        <Header />
      </GridNav>

      <GridSidebar>
        <Sidebar />
      </GridSidebar>
      <Main>
        <MainCadLeaguer/>
      </Main>
    </Container>
  );
};

export default CadLeaguers;
