import styled from "styled-components"

export const Container = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    
    
    h1 {
        color: #FFFFFF;
        font-size: 5em;
        font-weight: bold;
    }
    span {
        color: #FFFFFF;
        font-size: 3em;
        font-weight: bold;
    }
    h2 {
        display: flex;
        justify-content: center;
        align-items: flex-end;
        color: #FFFFFF;
        width: 120px;
        height: 120px;
        padding: 8px;
        font-size: 5em;
        transform: rotate(90deg);
        border-radius: 50%;
        border: 2px solid #FFFFFF;
    }
`