import React from 'react';
import {Container} from './styled'


const NotFound = () => {
    
    return (
        <Container >
            <h1>NotFound <span>404</span></h1>
            <h2>: (</h2>
        </Container>
    );
}

export default NotFound;