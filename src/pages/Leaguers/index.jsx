import React, { useState, useEffect } from 'react'
import Header from '../../components/Header';
import Sidebar from '../../components/Sidebar'
import { Main, Container, GridNav, GridSidebar } from './styled'
import { Table } from 'antd';
import Avaliacao from '../../components/ModalAvaliacao';
import EditLeaguer from '../../components/ModalEditLeaguer';

import api from '../../services/api';


const Leaguers = () => {

  const [results, setResults] = useState([]);
  
  const token = localStorage.getItem('token');
  const user_id = localStorage.getItem("user_id");
  const user = localStorage.getItem("user"); 

  useEffect(() => {
    api.get('leaguers', {
      headers: {
        "Authorization": `Bearer ${token}`
      }
    }).then(response => {
      setResults(response.data.Leaguers)
    })
  }, [token,results]);

  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      filters: results.map(res => {
        return ({
          text: `${res.name}`,
          value: `${res.name}`,
        })
      }),
      onFilter: (value, record) => {
        return record.name.startsWith(value)
      },
      filterSearch: true,
      width: '30%',
    },
    {
      title: 'Turma',
      dataIndex: 'turma',
      filters: results.map(res => {
        return ({
          text: `${res.turma_id.name}`,
          value: `${res.turma_id.name}`,
        })
      }),
      onFilter: (value, record) => record.turma.startsWith(value),
      filterSearch: true,
      width: '10%',
    },
    {
      title: 'Fases',
      dataIndex: 'fase',
      filters: results.map(res => {
        return ({
          text: `${res.fase}`,
          value: `${res.fase}`,
        })
      }),
      onFilter: (value, record) => {
        return record.fase.startsWith(value)
      },
      filterSearch: true,
      width: '10%',
    },
    {
      title: 'Responsavel',
      dataIndex: 'responsavel',
      filters: results.map(res => {
        return ({
          text: `${res.user_id.name}`,
          value: `${res.user_id.name}`,
        })
      }),
      onFilter: (value, record) => record.responsavel.startsWith(value),
      filterSearch: true,
      width: '10%',
    },
    {
      filter: () => results.map(res =>
      ({
        text: `${res._id}`,
        value: `${res._id}`,

      }),
      ),
    },
    {
     
      filter: () => results.map(res => {
        return ({
          text: `${res.user_id._id}`,
          value: `${res.user_id._id}`,
        })
      }),
    },
    {
      title: 'Envio de Avaliações',
      dataIndex: '',
      key: 'x',
      render: (record) => <Avaliacao id={record.ava} />,
    },
    {
      title: 'Editar Leaguer',
      dataIndex: '',
      key: 'x',
      render: (record) => <EditLeaguer id={record.id} turma={record.turma}/>,
    },
  ];
  const dataFilter = results.filter(res => res.user_id._id === user_id)
  .map((res) => {
    return ({
      name: `${res.name}`,
      turma: `${res.turma_id.name}`,
      fase: `${res.fase}`,
      responsavel: `${res.user_id.name}`,
      id: `${res._id}`,
      ava: `${res.user_id._id}`

    })
  });
  const data = results.map((res) => {
    return ({
      name: `${res.name}`,
      turma: `${res.turma_id.name}`,
      fase: `${res.fase}`,
      responsavel: `${res.user_id.name}`,
      id: `${res._id}`,
      ava: `${res.user_id._id}`
     

    })
  });
  const onChange = (pagination, filters, extra) => {
    console.log('params', pagination, filters, extra);
  };

  return (
    <Container>
      <GridNav>
        <Header />
      </GridNav>

      <GridSidebar>
        <Sidebar />
      </GridSidebar>
      <Main>
        <Table columns={columns} dataSource={user === 'Gestor' ? dataFilter : data} onChange={onChange} />
      </Main>
    </Container>
  )
}

export default Leaguers;