import { createGlobalStyle } from 'styled-components'
import 'antd/dist/antd.min.css'


export const GlobalStyled = createGlobalStyle `
  * {
    font-family: 'Montserrat', sans-serif;
  }  
  body {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    background-color: #122870;
  }
.ant-modal-content{
  background-color: #01579b;
  border-radius: 26px;
  
}
.ant-btn-primary {
  background-color: #122870;
  border: 0;
}
.ant-table-thead> tr >th{
  color: white;
  background: #0155a7 !important;
}
.ant-modal-header {
  text-align: center;
  font-size: 2em;
  border-radius: 26px 26px 0 0;
  background: #0155a7 !important;
}
.ant-message {
  top: 100px;
  left: 120px
}

.ant-modal-title {
  color: #ffffff
}
`
