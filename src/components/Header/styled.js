import styled from 'styled-components'

export const Header = styled.div`
  width: 100%;
  display: flex;
  height: 80px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background-color: #0155a7;
  padding: 2px 20px;
`

export const Img = styled.img`
  width: 10%;
  align-items: center;
`

export const H3 = styled.h3`
  color: #FFFFFF;
  font-size: 28px;  
  font-weight: 400;
`
