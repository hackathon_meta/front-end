import React from 'react';
import {Header, Img, H3  } from './styled';
import Logo from "../../assets/Logo-Meta.png";



const HeaderApp = () => {

        return (
            <Header>                
                  <Img src= {Logo}></Img>
                  <H3> Feed Leaguers</H3>                
            </Header>
        );
    }

export default HeaderApp;
