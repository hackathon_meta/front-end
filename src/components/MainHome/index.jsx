import React, { useState } from "react";
import { ContainerPage, SectionLeft, SectionRight, Img, P, ContainerLogin, H3, Span, Button, Form } from "./styled";
import Bubble from "../../assets/Frame 10.svg";
import logoLogin from "../../assets/image_11.png";

import api from '../../services/api'

import { useNavigate } from 'react-router-dom'

import { Input, message } from 'antd';

import { EyeInvisibleOutlined, EyeTwoTone, MailOutlined, LockOutlined } from '@ant-design/icons';

const MainHome = () => {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const history = useNavigate();

  const key = 'updatable';

  const handleLogin = async (e) => {
    e.preventDefault();

    const data = { email, password };

    if (email === '' || password === '') {

      message.loading({ content: 'Processando...', key });
      setTimeout(() => {
        message.warning({ content: 'Digite Email e Senha', key, duration: 2 });
      }, 1000);
    }
    else {

      try {
        message.loading({ content: 'Processando...', key });
        const response = await api.post('/login', data);
        const { token, } = response.data;
        const { _id, user } = response.data.user;
        console.log(_id, user)

        localStorage.setItem('token', token);
        localStorage.setItem('user_id',_id);
        localStorage.setItem('user',user);
        setTimeout(() => {
          message.success({ content: 'Acesso Liberado', key, duration: 2 });

          history('/leaguers');
        }, 1000);

      } catch (error) {

        message.loading({ content: 'processando...', key });
        setTimeout(() => {
          message.error({ content: 'Email ou Senha Inválido', key, duration: 2 });
        }, 1000);
      };

    }

  }

  return (
    <ContainerPage>
      <SectionLeft>
        <P>
          “Todos nós precisamos de pessoas que nos dêem feedback. <br />É
          assim que melhoramos.”
        </P>

        <Img src={Bubble}></Img>
      </SectionLeft>

      <SectionRight>
        <ContainerLogin>
          <img src={logoLogin} alt="logotipo Meta" />
          <H3>
            <strong>Bem vindo !</strong>
          </H3>
          <Span>faça o seu login</Span>
          <Form onSubmit={handleLogin}>
            <br />
            <br />
            <Input
              placeholder="Seu Email"
              prefix={<MailOutlined />}
              type="email"
              value={email}
              onChange={e => setEmail(e.target.value)}
              name="email"
            />
            <br />
            <Input.Password
              placeholder="Seu Password"
              iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
              prefix={<LockOutlined />}
              type="password"
              value={password}
              onChange={e => setPassword(e.target.value)}
              name="password"
            />

            <Button type="submit">ENTRAR</Button>
          </Form>
        </ContainerLogin>
      </SectionRight>
    </ContainerPage>
  );
}

export default MainHome;