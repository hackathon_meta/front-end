import styled from 'styled-components';

export const ContainerPage = styled.div` 
    display: grid;
    grid-template-columns: 1fr 1fr;
    width: 100%;

`
export const P = styled.p`
    display: flex;
    flex-direction: column;
    width: 65%;
    font-size: 35px;
    font-weight: 600;
    color: #dffe42;
`

export const SectionLeft = styled.div`       
    height: 82vh;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`

export const SectionRight = styled.div`
    width: 100%;
    height: 82vh;
`

export const Img = styled.img`
    width: 55%;
    
`

export const ContainerLogin = styled.div`
position:absolute;
bottom:0;
right:89px;
width: 30%;
height:65vh;
display:flex;
justify-content:center;
flex-direction:column;
align-items:center;
border:transparent;
border-radius: 8px 8px 0 0;
background-color: #fff;
`
export const H3 = styled.h3`
font-family: 'Open Sans', sans-serif;
`

export const Form = styled.form`
 display: flex;
 flex-direction: column;
 align-items: center;
 justify-content: center;
 width:80%;
`

export const Button = styled.button`
width:100%;
border-radius:3px;
border-color:#636B6F;
color:#ffffff;
background-color:#122870;
padding: 8px;
margin: 10px;


:hover{
    color:#ffff;
    font-weight: bold;
    cursor: pointer;
}
`

export const Span = styled.span`
margin-top:-15px;
font-family: 'Open Sans', sans-serif;
`

export const ImgLogo = styled.img`
display:flex;
width:100%;
`