import React from 'react';
import * as IoIcons from 'react-icons/io';
import  { AiOutlineCalendar, AiOutlineSafety }  from 'react-icons/ai';

const user = localStorage.getItem("user"); 

export const SidebarData = [
  {
    title: 'Leaguers',
    path: `${'/leaguers'}`,
    icon: <AiOutlineSafety />,
    cName: `${'enable'}`
  },
  {
    title: 'Cadastrar Leaguers',
    path: `${user === 'Gestor' ? '/leaguers' :'/cadleaguers'}`,
    icon: <IoIcons.IoMdPeople />,
    cName: `${user !== 'Gestor' ? 'enable': 'disable'}`
  },
  {
    title: 'Usuários do Sistema',
    path: `${user !== 'Admin' ? '/leaguers' :'/listusers'}`,
    icon: <AiOutlineCalendar />,
    cName: `${user === 'Admin' ? 'enable':'disable'}`
  },
  
];