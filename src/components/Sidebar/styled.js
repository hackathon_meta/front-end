import styled from 'styled-components';

export const Container = styled.div`
    background-color: #0155a7;
    height: 100%; //altura do sidebar 
    width: 100%;
    display: flex;
    flex-direction: column;
    box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;   

    
`
export const Ul = styled.ul`
    padding-left: 0;
    margin: 0;
    margin-top: 20px;
    width: 15vw;
    display: flex;
    justify-content: flex-start;
    flex-direction: column;
    list-style: none;
`

export const Li = styled.li`
    display: flex;
    justify-content: space-between;
    color: white;
    // margin: 10px, 0;
    min-width: 105%;
    cursor: pointer;

    :hover{
        color:#ffffff;
        font-weight: bold;
        cursor: pointer;
        background-color: #0162c0;
    }
`

export const Span = styled.div`
 min-width: 100%;
 display: flex;
 flex-direction: row;
//  justify-content: space-between;
 align-items: center;
 color: #ffffff;
 font-size: 16px;
 span {
    margin: 10px;
 } 
 .disable {
    opacity:0.6;
    cursor: not-allowed; 

 }
 .enable {
     
    pointer-events:all; //This makes it not clickable
    opacity:null; 
 }
`

export const ButtonExit = styled.button`       
    height: 40px;
    width: 180px;
    margin: 250px 10px 0px 30px;
    cursor: pointer;
    font-size: 20px;
    font-style: normal;
    line-height: 195.28%;
    color: #FFFFFF;
    top: 800px;
    background: linear-gradient(90deg, #0334D7 0%, #243B55 100%);
    border-radius: 10px;
    border: none;
    display: flex;
    justify-content: space-evenly;

    :hover{
        color:#ffffff;
        font-weight: bold;
        cursor: pointer;
        background: linear-gradient(90deg, #0334D7 10%, #243B55 90%);
    }
`
export const Img = styled.img`
  padding: 5px;
`

export const User = styled.div`
 display: flex;
 flex-direction: column;
 align-items: center;
 background-color : #0155a7;
 border-bottom: solid 2px #62adf7;
 padding-top: 15px;
`

export const P = styled.p`
color: #ffffff;
font-size: 20px;
display: flex;
margin-bottom: 15px;
`
export const Roles = styled.span`
background-color: #ffffff;
color : #0155a7;
font-size: 20px;
display: flex;
margin-bottom: 15px;
padding: 2px 10px;
border-radius: 5px;
`