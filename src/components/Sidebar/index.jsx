import React, {useState, useEffect} from "react";
import { Link } from "react-router-dom";
import { Container, Ul, Li, Span, ButtonExit, Img, User, P, Roles } from "./styled";
import { SidebarData } from "./SidebarData";
import Sair from "../../assets/upload.png";
import IconePerfil from "../../assets/Frame 12.png";
import { useNavigate } from "react-router-dom";

import api from "../../services/api";

const Sidebar = () => {
  const history = useNavigate();
  const [user, setUser] = useState([])
  const [sidebar] = useState(SidebarData)

  const handleLogout = () => {
    localStorage.clear();

    history("/");
  };

  const user_id = localStorage.getItem("user_id");
  const token = localStorage.getItem('token');
  const roles = localStorage.getItem("user"); 

  useEffect(() => {

    listUserId(user_id)
   

  }, [user_id]);


  const listUserId = async () => {
    const response = await api.get(`user/${user_id}`, {
      headers: {
        Authorization: `Bearer ${token}`,

      }
    })
    setUser([response.data.userId])
    
  }
  return (
    <Container>
      <User> 
        <Img alt="Icone pessoa" src={IconePerfil}></Img>
        {user.map((res)=> (
          <P>{`Olá ${res.name} `}</P>
          
        ))}
        <Roles>{roles}</Roles>
      </User>

      <Ul>
        {sidebar.map((res, key) => (
          <>
          <Link to={res.path}>
            <Li key={key}>
              <Span>
                <span className={res.cName}>{res.icon}</span>
                <span className={res.cName}>{res.title}</span>
              </Span>
            </Li>
          </Link>
          </>
        ))}
      </Ul>
      <ButtonExit
        onClick={() => {
          handleLogout();
        }}
      >
        <p>Sair</p>
        <Img alt="saida" src={Sair}></Img>
      </ButtonExit>
    </Container>
  );
};

export default Sidebar;
