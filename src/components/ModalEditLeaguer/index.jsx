import React, { useState, useEffect } from "react";
import { Modal, Tooltip, Select, Input, Tag } from "antd";
import { Button, Form } from "./styled";

import api from '../../services/api';

const tagRender = (props) => {
  const { label, closable, onClose } = props;

  const onPreventMouseDown = (event) => {
    event.preventDefault();
    event.stopPropagation();
  };

  return (
    <Tag
      color={"blue"}
      onMouseDown={onPreventMouseDown}
      closable={closable}
      onClose={onClose}
      style={{
        marginRight: 3,
      }}
    >
      {label}
    </Tag>
  );
};

const EditLeaguer = (props) => {
  const [visible, setVisible] = useState(false);
  const [data, setData] = useState([]);
  const [leaguer, setLeaguer] = useState({
    name: '',
    email: '', 
    fase: '', 
    tecnologias: [], 
    idiomas: [], 
    turma_id: '',
    user_id: ''
  })
  const { Option, OptGroup } = Select;

  const options1 = [
    {
      value: "React.js",
    },
    {
      value: "HTML",
    },
    {
      value: "Javascript",
    },
    {
      value: "Css",
    },
    {
      value: "Node.js",
    },
    {
      value: "",
    },
  ];

  const options2 = [
    {
      value: "Português",
    },
    {
      value: "Alemão",
    },
    {
      value: "Espanhol",
    },
    {
      value: "Francês",
    },
    {
      value: "Inglês",
    },
    {
      value: "Italiano",
    },
  ];
  
  const token = localStorage.getItem('token');
  const roles = localStorage.getItem('user');

  useEffect(() => {

    handleEdit(props.id)
    api.get('turmas', {
      headers: {
        "Authorization": `Bearer ${token}`
      }
    }).then(response => {
      setData(response.data.Turmas);
    })

  }, []);
  
  const onSubmit = async () => {

    await api.put(`leaguer/${props.id}`, leaguer, {
      headers: {
        Authorization: `Bearer ${token}`,

      }
    })
    setVisible(false)
    handleEdit()
    await api.post('leaguer', leaguer, {
      headers: {
        Authorization: `Bearer ${token}`,

      }

    })
  }
  const updateModel = (e) => {
    setLeaguer({
      ...leaguer,
      [e.target.name]: e.target.value
    })
  }
  const selectInputTurma = (value) => {
    console.log(value)
    setLeaguer({
      ...leaguer,
      turma_id: value,
      
    })
  }
  const selectInputFase = (value) => {
    console.log(value)
    setLeaguer({
      ...leaguer,
      fase: value
      
    })
  }
  const selectInputTec = (value) => {
    console.log(value)
    setLeaguer({
      ...leaguer,
      tecnologias: value
      
    })
  }
  const selectInputIdiomas = (value) => {
    console.log(value)
    setLeaguer({
      ...leaguer,
      idiomas: value
      
    })
  }
  const handleEdit = async (id) => {
    const response = await api.get(`leaguer/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,

      }
    })
    setLeaguer(response.data.leaguerId)

  }

  return (
    <>
      <Tooltip placement="left">
        <Button 
          style={{
            pointerEvents: roles === 'Gestor' ? 'none' : 'all', 
            opacity: roles === 'Gestor' ? 0.6 : 100,
           }} 
          onClick={() => setVisible(true)}>
          Editar
        </Button>
      </Tooltip>
      <Modal
        width="30%"
        title={'Editar Leaguer'}
        centered
        bodyStyle={{ height: "60vh" }}
        visible={visible}
        onOk={() => {onSubmit() }}
        onCancel={() => setVisible(false)}
      >
        <Form>
          <h3 style={{ color: "white" }}>
            Editar</h3>
          <Input
            type="text"
            placeholder="Nome"
            id="name"
            name="name"
            value={leaguer.name}
            onChange={ e => updateModel(e)}
            style={{
              width: "80%",
              margin: "12px",
            }}
            size="large"
          />
          <Select
          defaultValue={props.turma}
            placeholder="Turma"
            style={{
              width: "80%",
              margin: "12px",
            }}
            onChange={ e => selectInputTurma(e)}
            size="large"
          >
            <OptGroup>
              {data.map((res) => (
                <Option value={res._id}>{res.name}</Option>
              ))}
              
            </OptGroup>
          </Select>
          <Select
            placeholder="Fase"
            defaultValue={leaguer.fase}
            style={{
              width: "80%",
              margin: "12px",
            }}
            onChange={ e => selectInputFase(e)}
            size="large"
          >
            <OptGroup>
              <Option value="Intro">Intro</Option>
              <Option value="Labs">Labs</Option>
              <Option value="Beta">Beta</Option>
            </OptGroup>
          </Select>
          <Select
            placeholder="Tecnologias"
            mode="multiple"
            showArrow
            tagRender={tagRender}
            defaultValue={leaguer.tecnologias}
            onChange={ e => selectInputTec(e)}
            style={{
              width: "80%",
              margin: "12px",
            }}
            options={options1}
          />
          <Select
            placeholder="Idiomas"
            mode="multiple"
            showArrow
            tagRender={tagRender}
            defaultValue={leaguer.idiomas}
            onChange={ e => selectInputIdiomas(e)}
            style={{
              width: "80%",
              margin: "12px",
            }}
            options={options2}
          />
        </Form>
      </Modal>
    </>
  );
};

export default EditLeaguer;
