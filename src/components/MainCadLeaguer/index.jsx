import React, { useState, useEffect } from "react";
import { Container, HeaderCadLeaguer } from "./styled";
import { Form, message, Select, Input, Tag } from "antd";

import api from '../../services/api'

const tagRender = (props) => {
  const { label, closable, onClose } = props;

  const onPreventMouseDown = (event) => {
    event.preventDefault();
    event.stopPropagation();
  };

  return (
    <Tag
      color={"blue"}
      onMouseDown={onPreventMouseDown}
      closable={closable}
      onClose={onClose}
      style={{
        marginRight: 3,
      }}
    >
      {label}
    </Tag>
  );
};

const MainCadLeaguer = () => {
  const [form] = Form.useForm();
  const [dataTurma, setDataTurma] = useState([]);
  const [dataUsers, setDataUsers] = useState([]);

  const [leaguer, setLeaguer] = useState({
    name: '',
    email: '',
    fase: '',
    tecnologias: [],
    idiomas: [],
    turma_id: '',
    user_id: ''
  })
  const token = localStorage.getItem('token');

  useEffect(() => {

    api.get('turmas', {
      headers: {
        "Authorization": `Bearer ${token}`
      }
    }).then(response => {
      setDataTurma(response.data.Turmas);
    })
    api.get('users', {
      headers: {
        "Authorization": `Bearer ${token}`
      }
    }).then(response => {
      setDataUsers(response.data.Users);
    })
  }, [token]);

  const handleRegister = async (e) => {
    
    const data = {
      name: leaguer.name,
      email: leaguer.email,
      fase: leaguer.fase,
      tecnologias: leaguer.tecnologias,
      idiomas: leaguer.idiomas,
      turma_id: leaguer.turma_id,
      user_id: leaguer.user_id,
    }
    const key = 'updatable'

    if (data === '') {

      message.info({ content: 'Preencha todos os campos.', key, duration: 3.5 });
    } else {

      try {
       
        await api.post('/leaguer', data, {
          headers: {
            Authorization: `Bearer ${token}`,

          }
        })

        message.loading({ content: 'Loading...', key });
        setTimeout(() => {
          message.success({ content: 'Leaguer cadastrado com sucesso.', key, duration: 3 });
        }, 1500);
        setTimeout(() => {
          window.location.reload()
        }, 1000);
        
      } catch {

        message.warning({ content: 'Erro, por favor tente novamente...', duration: 3 });
      }
    }
  }
  const { Option, OptGroup } = Select;

  const options1 = [
    {
      value: "React.js",
    },
    {
      value: "HTML",
    },
    {
      value: "Javascript",
    },
    {
      value: "Css",
    },
    {
      value: "Node.js",
    },
    {
      value: "",
    },
  ];

  const options2 = [
    {
      value: "Português",
    },
    {
      value: "Alemão",
    },
    {
      value: "Espanhol",
    },
    {
      value: "Francês",
    },
    {
      value: "Inglês",
    },
    {
      value: "Italiano",
    },
  ];
  const updateModel = (e) => {
    setLeaguer({
      ...leaguer,
      [e.target.name]: e.target.value
    })
  }
  const selectInputTurma = (value) => {
    console.log(value)
    setLeaguer({
      ...leaguer,
      turma_id: value,

    })
  }
  const selectInputFase = (value) => {
    console.log(value)
    setLeaguer({
      ...leaguer,
      fase: value

    })
  }
  const selectInputTec = (value) => {
    console.log(value)
    setLeaguer({
      ...leaguer,
      tecnologias: value

    })
  }
  const selectInputIdiomas = (value) => {
    console.log(value)
    setLeaguer({
      ...leaguer,
      idiomas: value

    })
  }
  const selectInputRoles = (value) => {
    console.log(value)
    setLeaguer({
      ...leaguer,
      user_id: value

    })
  }
  return (
    <Container>
      <HeaderCadLeaguer>
        <h1 className="h1"> Cadastrar Leaguer </h1>
      </HeaderCadLeaguer>

      <div className="main_cad">
        <Form className="form" form={form}>
          <Form.Item name="nome" value={leaguer.name} rules={[{ required: true, },]}>
            <Input
              type="text"
              placeholder="Nome"
              name="name"
              onChange={e => updateModel(e)}
            />
          </Form.Item>
          <Form.Item name="Turma" rules={[{ required: true, },]}>
            <Select
              placeholder="Turmas"
              value={leaguer.turma_id}
              onChange={e => selectInputTurma(e)}
              size="large"
            >
              <OptGroup>
                {dataTurma.map((res) => (
                  <Option value={res._id}>{res.name}</Option>
                ))}

              </OptGroup>
            </Select>
          </Form.Item>
          <Form.Item name="Fase" rules={[{ required: true }]}>
            <Select
              placeholder="Fases"
              onChange={e => selectInputFase(e)}
              size="large"
              value={leaguer.fase}
            >
              <OptGroup>
                <Option value="Intro">Intro</Option>
                <Option value="Labs">Labs</Option>
                <Option value="Beta">Beta</Option>
              </OptGroup>
            </Select>
          </Form.Item>
          <Form.Item name="Tecnologias" rules={[{ required: true }]}>
            <Select
              placeholder="Tecnologias"
              mode="multiple"
              value={leaguer.tecnologias}
              showArrow
              tagRender={tagRender}
              defaultValue={leaguer.tecnologias}
              onChange={e => selectInputTec(e)}
              options={options1}
            />
          </Form.Item>
          <Form.Item name="Idiomas" rules={[{ required: true }]}>
            <Select
              placeholder="Idiomas"
              mode="multiple"
              showArrow
              value={leaguer.idiomas}
              tagRender={tagRender}
              defaultValue={leaguer.idiomas}
              onChange={e => selectInputIdiomas(e)}
              options={options2}
            />
          </Form.Item>
          <Form.Item name="Responsavel" rules={[{ required: true, }]}>
            <Select
              placeholder="Responsável"
              value={leaguer.user_id}
              onChange={e => selectInputRoles(e)}
              size="large"
            >
              <OptGroup>
                {dataUsers.map((res) => (
                  <Option value={res._id}>{res.name}</Option>
                ))}

              </OptGroup>
            </Select>
          </Form.Item>
          <Form.Item name="E-mail" rules={[{ required: true, }]}>
            <Input
              type="text"
              placeholder="E-mail"
              name="email"
              value={leaguer.email}
              onChange={e => updateModel(e)}
            />
          </Form.Item>
          <div className="form_button">
            <button type="submit" onClick={() => handleRegister()}>Cadastrar</button>
          </div>
        </Form>
      </div>
    </Container>
  );
};

export default MainCadLeaguer;
