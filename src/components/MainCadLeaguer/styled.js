import styled from "styled-components";

export const Container = styled.div`
  margin: 0px 0px 0px 122px;
  padding: 20px;
  // height: 83vh;
  background-color: #ffffff;
  font-family: "Poppins";
  font-style: normal;
  font-weight: 600;
  border-top-left-radius: 15px;
  border-top-right-radius: 15px;

  .main_cad {
    margin: 0 auto;
    color: #000000;
    height: 75%;
    width: 70%;
    text-align: center;
    background: #ffffff;
  }
  .form {
    margin-top: 40px;
    margin-bottom: 8px;
    display: flex;
    flex-direction: column;
  }
  .form Input {
    padding: 8px;
    height: 40px;
    width: 100%;
    font-size: 14px;
    font-style: normal #1077db;
    font-weight: bold;
    line-height: 195.28%;
    color: #1077db;
    text-align: center;
  }
  .form_button button {
    height: 50px;
    cursor: pointer;
    font-size: 20px;
    font-style: normal;
    line-height: 195.28%;
    color: #ffffff;
    width: 324px;
    left: 178px;
    top: 579px;
    background: linear-gradient(90deg, #0334d7 0%, #243b55 100%);
    border-radius: 10px;

    :hover {
      color: #ffffff;
      font-weight: bold;
      cursor: pointer;
      background: linear-gradient(90deg, #0334d7 10%, #243b55 90%);
    }
  }
`
export const HeaderCadLeaguer = styled.div`
  margin: -20px;  
  background-color: #0155a7;  
  border-top-left-radius: 15px;
  border-top-right-radius: 15px;

  .h1 {
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 10px;
    color: #ffffff;
    font-style: bold;
    font-weight: 600;
  }
`