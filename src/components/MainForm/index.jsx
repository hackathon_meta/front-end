import React from "react";
import { Container, HeaderForm } from "./styled";
import { Select, Input, Tag, DatePicker, Space  } from "antd";

const MainForm = () => {
  const { TextArea } = Input;

  const onChangeText = (e) => {
    console.log("Change:", e.target.value);
  };

  const handleChange = (value) => {
    console.log(`selected ${value}`);
  };

  const { Option, OptGroup } = Select;

  const projetos = [
    {
      value: "Meta Skills",
    },
    {
      value: "Meta People",
    }
  ];

  const Tecnologias = [
    {
      value: "PHP",
    },
    {
      value: "Laravel",
    },
    {
      value: "Javascript",
    },
    {
      value: "Vue.JS",
    },
    {
      value: "MySql",
    },
    {
      value: "Node.JS",
    },
    {
      value: "Adonis.JS",
    },
    {
      value: "PostgreSQL",
    },
    {
      value: "Versionamento com Git",
    },
    {
      value: "ReactJS",
    },
    {
      value: "HTML",
    },
    {
      value: "CSS",
    }
  ];
   
  const onChangeDate = (date, dateString) => {
    console.log(date, dateString);
  };

  const tagRender = (props) => {
    const { label, closable, onClose } = props;
  
    const onPreventMouseDown = (event) => {
      event.preventDefault();
      event.stopPropagation();
    };

    return (
      <Tag
        color={"blue"}
        onMouseDown={onPreventMouseDown}
        closable={closable}
        onClose={onClose}
        style={{
          marginRight: 3,
        }}
      >
        {label}
      </Tag>
    );
  };



  return (
    <Container>
      <HeaderForm>
        <h1 className="h1"> Formulário de Feedback </h1>
      </HeaderForm>

      <div className="main_form">
      <form className="form">
        <h3 className="h3">Profissional</h3>
        <Input
         name="Profissional" 
         label="Profissional" 
         style={{
           width: '85%',
         }}  
        />

        <h3 className="h3">Projeto(s)</h3>
        <Select
         mode="multiple"
         showArrow
         tagRender={tagRender}
         defaultValue={[]}
         style={{
          width: '85%',
        }}
         options={projetos}
        />

        <h3 className="h3">Tecnologias</h3>
        <Select
         mode="multiple"
         showArrow
         tagRender={tagRender}
         defaultValue={[]}
         style={{
          width: '85%',
        }}
         options={Tecnologias}
        />

        <h3 className="h3" >Tempo de formação</h3>
        <Select
          name=""
          label=""
          defaultValue=""
          style={{
            width: '85%',
          }}
          onChange={handleChange}
        >
          <OptGroup>
            <Option value="Menos de 6 Meses">Menos de 6 Meses</Option>
            <Option value="Mais de 6 Meses">Mais de 6 Meses</Option>
            <Option value="Mais de 1 Ano">Mais de 1 Ano</Option>
          </OptGroup>
        </Select>

        <h3 className="h3" >Papel</h3>
        <Select
          name=""
          label=""
          defaultValue=""
          style={{
            width: '85%',
          }}
          onChange={handleChange}
        >
          <OptGroup>
            <Option value="Desenvolvedor Frontend">
              Desenvolvedor Frontend
            </Option>
            <Option value="Desenvolvedor Backend">Desenvolvedor Backend</Option>
            <Option value="Testador de Software">Testador de Software</Option>
            <Option value="Designer Gráfico (UX / UI)">
              Designer Gráfico (UX / UI)
            </Option>
            <Option value="Scrum Master">Scrum Master</Option>
            <Option value="Product Owner">Product Owner</Option>
            <Option value="Analista de Negócios">Analista de Negócios</Option>
            <Option value="Analista de Requisitos">
              Analista de Requisitos
            </Option>
          </OptGroup>
        </Select>

        <h3 className="h3" >Modelo de contratação</h3>
        <Select
          name=""
          label=""
          defaultValue=""
          style={{
            width: '85%',
          }}
          onChange={handleChange}
        >
          <OptGroup>
            <Option value="CLT">CLT</Option>
            <Option value="P.J.">P.J.</Option>
          </OptGroup>
        </Select>

        <h3 className="h3">Demonstrou performance na entrega ?</h3>
        <Select
          name=""
          label=""
          defaultValue=""
          style={{
            width: '85%',
          }}
          onChange={handleChange}
        >
          <OptGroup>
            <Option value="Acima da Média">Acima da Média</Option>
            <Option value="Regular">Regular</Option>
            <Option value="Abaixo da Média">Abaixo da Média</Option>
            <Option value="Pode ser desenvolvido">Pode ser desenvolvido</Option>
          </OptGroup>
        </Select>
        <TextArea 
        showCount maxLength={200} 
        onChangeText={onChangeText}
        style={{
          width: '85%',
        }}
        />

        <h3>Entregou com qualidade ?</h3>
        <Select
          name=""
          label=""
          defaultValue=""
          style={{
            width: '85%',
          }}
          onChange={handleChange}
        >
          <OptGroup>
            <Option value="Acima da Média">Acima da Média</Option>
            <Option value="Regular">Regular</Option>
            <Option value="Abaixo da Média">Abaixo da Média</Option>
            <Option value="Pode ser desenvolvido">Pode ser desenvolvido</Option>
          </OptGroup>
        </Select>
        <TextArea 
        showCount maxLength={200} 
        onChangeText={onChangeText}
        style={{
          width: '85%',
        }}
        />

        <h3>Profissional proativo ?</h3>
        <Select
          name=""
          label=""
          defaultValue=""
          style={{
            width: '85%',
          }}
          onChange={handleChange}
        >
          <OptGroup>
            <Option value="Acima da Média">Acima da Média</Option>
            <Option value="Regular">Regular</Option>
            <Option value="Abaixo da Média">Abaixo da Média</Option>
            <Option value="Pode ser desenvolvido">Pode ser desenvolvido</Option>
          </OptGroup>
        </Select>
        <TextArea
          showCount
          maxLength={200}
          onChangeText={onChangeText}
          label=""
          style={{
            width: '85%',
          }}
        />

        <h3>Demonstrou comprometimento ?</h3>
        <Select
          name=""
          label=""
          defaultValue=""
          style={{
            width: '85%',
          }}
          onChange={handleChange}
        >
          <OptGroup>
            <Option value="Acima da Média">Acima da Média</Option>
            <Option value="Regular">Regular</Option>
            <Option value="Abaixo da Média">Abaixo da Média</Option>
            <Option value="Pode ser desenvolvido">Pode ser desenvolvido</Option>
          </OptGroup>
        </Select>
        <TextArea
          showCount
          maxLength={200}
          onChangeText={onChangeText}
          style={{
            width: '85%',
          }}
        />

        <h3>Sabe trabalhar em equipe ?</h3>
        <Select
          name=""
          label=""
          defaultValue=""
          style={{
            width: '85%',
          }}
          onChange={handleChange}
        >
          <OptGroup>
            <Option value="Acima da Média">Acima da Média</Option>
            <Option value="Regular">Regular</Option>
            <Option value="Abaixo da Média">Abaixo da Média</Option>
            <Option value="Pode ser desenvolvido">Pode ser desenvolvido</Option>
          </OptGroup>
        </Select>
        <TextArea
          showCount
          maxLength={200}
          onChangeText={onChangeText}
          style={{
            width: '85%',
          }}
         
        />

        <h3>Desenvolveu suas habilidades ?</h3>
        <Select
          name=""
          label=""
          defaultValue=""
          style={{
            width: '85%',
          }}
          onChange={handleChange}
        >
          <OptGroup>
            <Option value="Acima da Média">Acima da Média</Option>
            <Option value="Regular">Regular</Option>
            <Option value="Abaixo da Média">Abaixo da Média</Option>
            <Option value="Pode ser desenvolvido">Pode ser desenvolvido</Option>
          </OptGroup>
        </Select>
        <TextArea
          showCount
          maxLength={200}
          onChangeText={onChangeText}
          style={{
            width: '85%',
          }}
        />

        <h3>Possui capacidade de liderança ?</h3>
        <Select
          name=""
          label=""
          defaultValue=""
          style={{
            width: '85%',
          }}
          onChange={handleChange}
        >
          <OptGroup>
            <Option value="Acima da Média">Acima da Média</Option>
            <Option value="Regular">Regular</Option>
            <Option value="Abaixo da Média">Abaixo da Média</Option>
            <Option value="Pode ser desenvolvido">Pode ser desenvolvido</Option>
          </OptGroup>
        </Select>
        <TextArea
          showCount
          maxLength={200}
          onChangeText={onChangeText}
          style={{
            width: '85%',
          }}
        />

        <h3>É um profissional pontual ?</h3>
        <Select
          name=""
          label=""
          defaultValue=""
          style={{
            width: '85%',
          }}
          onChange={handleChange}
        >
          <OptGroup>
            <Option value="Acima da Média">Acima da Média</Option>
            <Option value="Regular">Regular</Option>
            <Option value="Abaixo da Média">Abaixo da Média</Option>
            <Option value="Pode ser desenvolvido">Pode ser desenvolvido</Option>
          </OptGroup>
        </Select>
        <TextArea 
        showCount maxLength={200} 
        onChangeText={onChangeText}
        style={{
          width: '85%',
        }}
        />

        <h3>Sabe trabalhar sob pressão ?</h3>
        <Select
          name=""
          label=""
          defaultValue=""
          style={{
            width: '85%',
          }}
          onChange={handleChange}
        >
          <OptGroup>
            <Option value="Acima da Média">Acima da Média</Option>
            <Option value="Regular">Regular</Option>
            <Option value="Abaixo da Média">Abaixo da Média</Option>
            <Option value="Pode ser desenvolvido">Pode ser desenvolvido</Option>
          </OptGroup>
        </Select>
        <TextArea 
         showCount maxLength={200} 
         onChangeText={onChangeText} 
           style={{
             width: '85%',
          }}
        />

        <h3>
          Participou ativamente das cerimônias (lições aprendidas-retrospectivas)?
        </h3>
        <Select
          name=""
          label=""
          defaultValue=""
          style={{
            width: '85%',
          }}
          onChange={handleChange}
        >
          <OptGroup>
            <Option value="Acima da Média">Acima da Média</Option>
            <Option value="Regular">Regular</Option>
            <Option value="Abaixo da Média">Abaixo da Média</Option>
            <Option value="Pode ser desenvolvido">Pode ser desenvolvido</Option>
          </OptGroup>
        </Select>
        <TextArea 
          showCount maxLength={200} 
          onChangeText={onChangeText} 
          style={{
            width: '85%',
          }}
          />

        <h3>Realizou atividades administrativas ?</h3>
        <Select
          name=""
          label=""
          defaultValue=""
          style={{
            width: '85%',
          }}
          onChange={handleChange}
        >
          <OptGroup>
            <Option value="Acima da Média">Acima da Média</Option>
            <Option value="Regular">Regular</Option>
            <Option value="Abaixo da Média">Abaixo da Média</Option>
            <Option value="Pode ser desenvolvido">Pode ser desenvolvido</Option>
          </OptGroup>
        </Select>
        <TextArea
          showCount
          maxLength={200}
          onChangeText={onChangeText}
           style={{
             width: '85%',
           }}
        />

        <h3>Quais caracteristicas que o profissional se destaca ?</h3>
        <TextArea
          showCount
          maxLength={300}
          onChangeText={onChangeText}
           style={{
             width: '85%',
           }}
         
        />

        <h3>Considerações gerais</h3>
        <TextArea
          showCount
          maxLength={300}
          onChangeText={onChangeText}
            style={{
              width: '85%',
           }}
        /> 
        <div>
           <h3>Data</h3>
           <DatePicker onChange={onChangeDate}/>
           <Input
            placeholder="Nome"
              style={{
               width: '30%',
              }}  
            />
            <Select       
              defaultValue=""
              style={{
                width: '30%',
              }}
              onChange={handleChange}
            >
             <OptGroup>
               <Option value="Cliente">Cliente</Option>
               <Option value="Colega de Equipe">Colega de Equipe</Option>
             </OptGroup>
           </Select>
         </div>        
      </form>
    </div> 

    <div className="form_button">
            <button>Enviar</button>
    </div>
    </Container>
  );
};
export default MainForm;
