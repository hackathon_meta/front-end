import styled from "styled-components";

export const Container = styled.div`
 margin: 40px 0px 0px 300px;
 width: 60%;
 background-color: #ffffff;
 font-family: "Poppins";
 font-style: normal;
 font-weight: 600; 
 border-radius: 15px;

  .form {
    margin: 40px; 
    display: flex;
    flex-direction: column;
    justify-content: center;
    width: auto; 
    align-items: center;
  }
   
  .h3 {
    margin-top: 15px;

  }

  .form_button button {
    margin: 0px 0px 40px 200px; 
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background: linear-gradient(90deg, #0334d7 0%, #243b55 100%);
    border-radius: 10px;
    color: #ffffff;
    cursor: pointer;
    font-size: 20px;
    font-style: normal;
    line-height: 195.28%;
    width: 324px;
    left: 178px;
    top: 579px;

    :hover {
      color: #ffffff;
      font-weight: bold;
      cursor: pointer;
      background: linear-gradient(90deg, #0334d7 10%, #243b55 90%);
    }
  }
`
export const HeaderForm = styled.div`
  background-color: #0155a7;  
  border-top-left-radius: 15px;
  border-top-right-radius: 15px;

  .h1 {
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 10px;
    color: #ffffff;
    font-style: bold;
    font-weight: 600;
  }
`

export const Space = styled.div`
 margin: 40px; 
 display: flex;
 flex-direction: column;
 justify-content: center;
 width: auto; 
 align-items: center;
`