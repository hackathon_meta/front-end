import React, { useState, useEffect } from "react";
import { Modal, Tooltip, Select, Input } from "antd";
import { Button, Form } from "./styled";
import api from "../../services/api";

const EditUser = (props) => {
  const [visible, setVisible] = useState(false);

  const { Option, OptGroup } = Select;

  const [user, setUser] = useState({
    name: '',
    cpf: '',
    email: '',
    user: '',
  })

  const token = localStorage.getItem('token');

  useEffect(() => {

    handleEdit(props.id)

  }, [props.id]);


  const onSubmit = async () => {
   
    await api.put(`user/${props.id}`, user, {
      headers: {
        Authorization: `Bearer ${token}`,

      }
    })
    
    setVisible(false)
    handleEdit()
  await api.post('user', user, {
    headers: {
      Authorization: `Bearer ${token}`,

    }
    
  })
}
  const updateModel = (e) => {
    setUser({
      ...user,
      [e.target.name]: e.target.value
    })
  }
  const saveGroup = (value) => {
    setUser({
      ...user,
      user: value
    })
  }
  const handleEdit = async (id) => {
    const response = await api.get(`user/${id}`, {
      headers: {
        Authorization: `Bearer ${token}`,

      }
    })
    setUser(response.data.userId)
    console.log(response.data.userId)

  }
  return (
    <>
      <Tooltip placement="left">
        <Button onClick={() => setVisible(true)}>Editar</Button>
      </Tooltip>
      <Modal
        width="30%"
        title={`Editar Usuario`}
        centered
        
        bodyStyle={{height:'50vh'}}
        visible={visible}
        onOk={() => {onSubmit()}}
        onCancel={() => setVisible(false)}        
      >
        <Form>
          <Input
            type="text"
            placeholder="Nome"
            id="name"
            name="name"
            value={user.name}
            onChange={ e => updateModel(e)}
            style={{
                width: "80%",
                margin: "12px",
            }}
            size="large" 
            
          />
          <Input
            type="text"
            placeholder="CPF"
            id="cpf"
            name="cpf"
            value={user.cpf}
            onChange={ e => updateModel(e)}
            style={{
                width: "80%",
                margin: "12px",
            }}
            size="large" 
            
          />
          <Input
            type="text"
            placeholder="email"
            id="email"
            name="email"
            value={user.email}
            onChange={ e => updateModel(e)}
            style={{
                width: "80%",
                margin: "12px",
            }}
            size="large" 
            
          />
          <Select
            defaultValue={user.user}
            onChange={ e => saveGroup(e)}
            style={{
                width: "80%",
                margin: "12px",
            }}
            
            size="large"
          >
            
            <OptGroup>
            <Option value="Admin" name="admin">Admin</Option>
              <Option value="Gestor" name="gestor">Gestor</Option>
              <Option value="Mentor" name="mentor">Mentor</Option>
            </OptGroup>
          </Select>
        </Form>
      </Modal>
    </>
  );
};

export default EditUser;
