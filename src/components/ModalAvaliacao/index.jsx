import React, { useState } from "react";
import { Modal, Tooltip, Select, Input } from "antd";
import {Container, Button, Form} from "./styled";
import { AiOutlineUsergroupAdd } from "react-icons/ai"

const Avaliacao = (props) => {
  const [visible, setVisible] = useState(false);

  const roles = localStorage.getItem('user');
  const user_id = localStorage.getItem('user_id');


  const { Option, OptGroup } = Select;

  const filterUserAvaliacao = (props.id === user_id)
  
  return (
    <Container >
      <Tooltip placement="left">
        <Button
        onClick={() => setVisible(true)}
        style={
          filterUserAvaliacao !== true && roles === 'Mentor' ? { pointerEvents:'none', opacity: 0.6,} :
           { pointerEvents:'all', opacity: 100,}}
        >Nova Avaliação
        </Button>
      </Tooltip>
      <Modal
        width="30%"
        centered
        bodyStyle={{ height: "50vh" }}
        visible={visible}
        onOk={() => {}}
        onCancel={() => setVisible(false)}
      >
        <Form>
        <h3 style={{color: "white"}}>
            Solicitar Feedback</h3>
          <Select
            
            placeholder="Nome"
            prefix={<AiOutlineUsergroupAdd />}
            style={{
              width: "80%",
              margin: "12px",              
            }}
            onChange={''}
            size="large"
          >
            <OptGroup>
              <Option value="Carlos Henrique Costa">Carlos Henrique Costa</Option>
              <Option value="Adrieli Lavratti">Adrieli Lavratti</Option>
              <Option value="Jaime Epifanio">Jaime Epifanio</Option>
            </OptGroup>
          </Select>

          <Input
            type="email"
            placeholder="Email"
            onChange={''}
            value={''}
            style={{
              width: "80%",
              margin: "12px",
            }}
            size="large"
          />

          <Select
            placeholder="Clientes"
            style={{
              width: "80%",
              margin: "12px",
            }}
            onChange={''}
            size="large"
          >
            <OptGroup>
              <Option value="Rede Globo">Rede Globo</Option>
              <Option value="Sicredi">Sicredi</Option>
              <Option value="Banco Original">Banco Original</Option>
            </OptGroup>
          </Select>
          
        </Form>
      </Modal>
    </Container>
  );
};

export default Avaliacao;
