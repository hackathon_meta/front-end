# 💬 **Projeto Feed Leaguers**💬<h2 id="topo"><h2>

Link do Vercel: -> 
 
- [Sobre](#sobre) 
- [Desafio](#desafio) 
- [O que funciona](#oquefunciona) 
- [Backlog](#backlog)
- [Tecnologias](#tecnologias)
- [Prints da Web](#printsdaWeb)
- [Desenvolvedores](#desenvolvedores) 


## Sobre 📓
 💻A ideia do projeto consiste em: 

Montar um sistema corporativo interno para a empresa Meta IT que realizasse as seguintes tarefas; a automação e informatização do processo de levantamento de indicadores quentes, a centralização e consolidação dos dados de feedback de respectivos Meta Leaguers e por fim manter o histórico e acompanhamento dos profissionais.

<a href="#topo">[Topo 🔝]</a>

##  Desafio 🏅 
- [x] Controle de perfis;
- [ ] Envio de e-mail;
- [x] Autenticação;
- [ ] Desenvolvimento de formulários;
- [x] Usabilidade e experiência do usuário.

<a href="#topo">[Topo 🔝]</a>

## O que funciona ✅

- Página Login:
    * É a página inicial do site, responsável por mostrar a o modal de login para a entrada do usuário no sistema.
<br></br>
- Página de Listagem de Leaguers:
    * Página responsável por mostrar a lista de leaguers que estão sobre a responsabilidade de: (Mentores e Gestores).
    * A página dá acesso a dois modais um para realizar uma nova avaliação de algum Meta Leaguer e outro modal que abre a possibilidade de Editar um Meta Leaguer.
<br></br>
- Página de Cadastrar Leaguers:
    * Página responsável por mostrar os inputs responsáveis por receber os dados de um novo Meta Leaguer.
<br></br>   
- Página de Usuários do Sistema:
    * Página responsável por mostrar a lista de usuários que são autorizados a mexer no sistema.
    * A página dá acesso a um modal um para realizar a mudança de cargo de algum usuário, entre (Mentores, Gestores, Admin).

<a href="#topo">[Topo 🔝]</a>

## Backlog ❎

-  Controle de Formulários 
- Envio do email com link de formulários.
- Responsividade, em algumas telas a configuração do sidebar e de onde renderiza a função alguns espaçamentos, ela desconfigura de acordo com a resolução da tela . 

<a href="#topo">[Topo 🔝]</a>

## Tecnologias 🛠️ 
As seguintes ferramentas foram usadas na construção do projeto:

* JavaScript 
* Styled-Components
* Ant Design
* React
* React icons
* React hooks
* React Router
* API

<a href="#topo">[Topo 🔝]</a>

##  Prints da Web 📷 

### Tela Login
![tela login](./src/assets/Tela_Login.png "Tela Login")

### Tela Listagem de Leaguers
![tela login](./src/assets/Tela_Listagem_de_Leaguers.jpg "Tela Login")

### Tela Cadastrar Leaguers
![tela login](./src/assets/Tela_Cadastrar_Leaguers.png "Tela Login")

### Tela Usuários do Sistema
![tela login](./src/assets/Tela_Edicao_Usuarios_do_Sistema.png "Tela Login")

<a href="#topo">[topo 🔝]</a>

## Desenvolvedores 🦸 

<a href="https://github.com/HEINRICK7">
 <img style="border-radius: 50%;" src="https://user-images.githubusercontent.com/14335370/153650468-2cdaf2d2-6ae6-47d3-b127-1ed03c1f6ca9.png" width="130px;" height="130px" alt="name"/>
 <br />
 <sub><b>Carlos Henrique</b></sub></a> <a href="https://github.com/HEINRICK7" title="gitHub">🚀</a>
 <br /> 
 
[![Linkedin Badge](https://img.shields.io/badge/-Henrique-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/heinrickcostta/)](https://www.linkedin.com/in/heinrickcostta/) 
---

<a href="https://github.com/Adritti2911">
 <img style="border-radius: 50%;" src="https://avatars.githubusercontent.com/u/94581344?v=4" width="130px;" height="130px" alt="name"/>
 <br />
 <sub><b> Adrieli Lavratti </b></sub></a> <a href="https://github.com/Adritti2911" title="gitHub">🚀</a>
 <br />

 [![Linkedin Badge](https://img.shields.io/badge/-Adrieli-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/adrielilf/)](https://www.linkedin.com/in/adrielilf/) 
---
 
 <a href="https://github.com/jaime-github">
 <img style="border-radius: 50%;" src="https://avatars.githubusercontent.com/u/77126556?v=4" width="130px;" height="130px" alt="name"/>
 <br />
 <sub><b> Jaime Epifanio </b></sub></a> <a href="https://github.com/jaime-github" title="gitHub">🚀</a>
 <br />
 
 [![Linkedin Badge](https://img.shields.io/badge/-Jaime-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/jaime-epifanio/)](https://www.linkedin.com/in/jaime-epifanio/) 
---

